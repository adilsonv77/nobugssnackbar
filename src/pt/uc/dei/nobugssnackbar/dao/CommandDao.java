package pt.uc.dei.nobugssnackbar.dao;

import pt.uc.dei.nobugssnackbar.model.Command;

public interface CommandDao extends Dao<Command> {

}
